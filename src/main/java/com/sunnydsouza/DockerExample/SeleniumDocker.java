package com.sunnydsouza.DockerExample;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class SeleniumDocker 
{
    public static void main( String[] args ) throws MalformedURLException
    {
       
        
        /**
         * For connecting nodes to the hub
		 * java -Dwebdriver.chrome.driver="<path-to-chromedriver-on-node-system>" -jar selenium-server-standalone-3.13.0.jar -role webdriver -hub http://10.83.8.165:4444/grid/register/ -port 5556
		 * 
		 * You need -role webdriver
		 * -hub <hub registering link>
		 * -port 5556
		 * 
		 * When using docker, when connecting to a node running on docker
		 * docker run --name node101 -p 5595:5555 -d -e HUB_PORT_4444_TCP_ADDR=10.83.101.31 -e HUB_PORT_4444_TCP_PORT=4444 -e SE_OPTS="-remoteHost http://10.83.101.31:5595" -e NODE_MAX_INSTANCES=5 -e NODE_MAX_SESSION=5  selenium/node-chrome:latest
		 * if creating multiple nodes in docker, use the below
		 * docker run --name node102 -p 5596:5555 -p 32300:5900 -d -e HUB_PORT_4444_TCP_ADDR=10.83.101.31 -e HUB_PORT_4444_TCP_PORT=4444 -e SE_OPTS="-remoteHost http://10.83.101.31:5596" -e NODE_MAX_INSTANCES=5 -e NODE_MAX_SESSION=5  selenium/node-chrome-debug:latest
		 * please note to keep the port for docker at 5555 only while changing the port exposed to the local machine
		 * 
		 * When using node-chrome-debug, you can use VNC viewer(chrome addon) to view the terminal of execution
		 * you can use the url i.e: in above example, 10.83.101.31:32300
		 * 
		 * to get into a docker container
		 * docker exec -it <mycontainer> bash
		 * 
		 * interactive mode
		 * docker run -it ubuntu /bin/bash

         */
        DesiredCapabilities capabilities1 = DesiredCapabilities.chrome();
	    capabilities1.setCapability("browserName", "chrome");
	    capabilities1.setCapability("platform", Platform.LINUX);
	    
	    WebDriver driver1=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),capabilities1);
	    driver1.get("https://google.in");
	    System.out.println(driver1.getTitle());
	    
	    DesiredCapabilities capabilities2 = DesiredCapabilities.firefox();
	    capabilities2.setCapability("browserName", "firefox");
	    capabilities2.setCapability("platform", Platform.LINUX);
	    
	    WebDriver driver2=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),capabilities2);
	    driver2.get("https://in.yahoo.com/?p=us");
	    System.out.println(driver2.getTitle());
    }
}
